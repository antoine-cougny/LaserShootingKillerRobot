/*_________________________________________________________
 * output function 
 * Christopher McClanahan, 11.2017
 * 
 * Motorcontroller for Embedded Systems Design Projct.
 * The outputs are written here. For safety the first bit in the statusbyte
 * (arm_bit) is checked before writing the outputs. If it is LOW all outputs are 
 * set to 0 and the rover brakes. 
 __________________________________________________________*/

void write_outputs(){
    Serial.print("Outputs");
    /*look if the motors should be armed */
    if(bitRead(Status_byte, 0) == 1){
        /*set motorspeed */
        analogWrite(Enable_A, abs(Output_Left));
        analogWrite(Enable_B, abs(Output_Right));  

        /*set Motor directions */
        if(Output_Left < 0) {
            digitalWrite(Input_A_1, LOW);
            digitalWrite(Input_A_2, HIGH); 
        }
        else{
            digitalWrite(Input_A_1, HIGH);
            digitalWrite(Input_A_2, LOW);
        }
        
        if(Output_Right < 0) {
            digitalWrite(Input_B_1, LOW);
            digitalWrite(Input_B_2, HIGH); 
        }
        else{
            digitalWrite(Input_B_1, HIGH);
            digitalWrite(Input_B_2, LOW);
        }
        
        /* indicate MC is armed */
        digitalWrite(LED_pin_RED, HIGH);
    }
    else {
        /* If armbit is LOW, stop all outputs of the H-bridges, Brake */
        digitalWrite(Enable_A, LOW);
        digitalWrite(Enable_B, LOW);
        digitalWrite(Input_A_1, LOW);
        digitalWrite(Input_A_2, LOW);
        digitalWrite(Input_B_1, LOW);
        digitalWrite(Input_B_2, LOW);

        /* indicate MC is not armed */
        digitalWrite(LED_pin_RED, LOW);
    }
}
