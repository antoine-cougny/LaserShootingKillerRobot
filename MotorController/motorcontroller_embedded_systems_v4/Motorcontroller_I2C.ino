/*_________________________________________________________
 * I2C function 
 * Christopher McClanahan, 10.2017
 * 
 * Data is recieved and added to the right address in the 
 * Storage array. After each transmission the transmissiontime is 
 * saved for the check_I2C_connection.
 * 
 * The data has to be sent in the following order: Throttle_Right, 
 * Throttle_Left, Arm_bit.
 __________________________________________________________*/

void receiveEvent(int howMany) {
    /*recieve I2C data and store it in the I2C_Storage array*/
    Serial.print("I2C_begin");
  
    uint8_t index = 2;
    while (Wire.available())
    {
        I2C_Storage[index--] = Wire.read();
    }

    Serial.print("I2C_end");    

    /*Set directions*/
    Status_byte = I2C_Storage[0];

    if(bitRead(Status_byte, 1) & B1){
        Throttle_Right =I2C_Storage[2] ;
    }
    else{
        Throttle_Right = I2C_Storage[2]*-1;
    }
    if(bitRead(Status_byte, 2)& B1){
        Throttle_Left = I2C_Storage[1]*-1;
    }
    else{
        Throttle_Left = I2C_Storage[1];
    }

    /*Set time of last transmission for "check_I2C_connection" */
    last_transmission = millis();


    /* for debugging */
    /*
    Serial.print("   Throttle_Left ");
    Serial.print(Throttle_Left);
    Serial.print("\t");
    Serial.print("   Throttle_Right ");
    Serial.print(Throttle_Right);
    Serial.print("\t");
    Serial.print("   Status_byte ");
    Serial.print(Status_byte, BIN);
    Serial.println("\t");
    */
}
