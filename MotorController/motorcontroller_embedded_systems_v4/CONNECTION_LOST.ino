/*_________________________________________________________
 * I2C connection lost function 
 * Christopher McClanahan, 10.2017
 * 
 * Motorcontroller for Embedded Systems Design Projct.
 * By checking the elapsed time since the last connection, this function 
 * determines if the connection is lost or not.
 * If the connection is lost, it stops the motoroutput, brakes and disarms the 
 * controller.
 __________________________________________________________*/

void check_I2C_connection() {
    //Serial.println("Checking I2C"); //for debugging
    
    //check elapsed time since the last I2C transmission
    connection_watchdog = millis() - last_transmission;
    
    if(connection_watchdog > max_time_without_command){
        //If I2C connection lost, disarm the MC and reset the direction bits
        Status_byte = 0;
        Serial.println("I2C lost");
        //Blink Red LED
        digitalWrite(LED_pin_RED, HIGH);
        delay(100);    
        digitalWrite(LED_pin_RED, LOW);
        delay(100);
    }
    Serial.print(connection_watchdog);
    Serial.println("\t");
}
