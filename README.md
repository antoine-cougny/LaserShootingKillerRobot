# LASER SHOOTING KILLER ROBOT
Heriot-Watt University - Edinburgh - 2017
B31VW - Embedded System Design Project

---
## Presentation of the Project
This idea of delopping a rover came from the wish to develop a modern game, that could be played either alone or with different opponents. As a team, our main aims were to build a robot high a high modularity thanks to attachements you can plug and instantly use, based on the ”plug’n’play” model of USB devices.

---
# Pictures

## Render of the Project
![Render of the Project](Images/lskr_render.jpg)

## Render with the different attachements
![Plug'n'Play Attachements](Images/lskr_attachements.jpg)

## Pictures of the rover
### Front
![Picture of the front](Images/rover_front.jpg)

### Top
![Picture of the top](Images/rover_top2.jpg)

### Back
![Picture of the back](Images/rover_back.jpg)


