package com.lskr.mmolskrgapplication.Robot;


import com.lskr.mmolskrgapplication.Constants;
import java.lang.Math;

/**
 * Created by Julien on 03/11/2017.
 * This class implement the robot object which is the main object of our app. 
 * It will work as * an interface between the UI and the RasPi by gathering all
 * data to be sent.
 */

public class Robot {

    private OrientationData orientationData;
    private String speed;
    private String fire;
    private String direction;
    private int shot;
    private int thrust;
    private int turret_pos;
    private int move;

    // Creator
    public Robot() {
        speed           = new String("0");
        direction       = new String("0");
        fire            = new String("0");
        shot            = 0;
        thrust          = 0;
        move            = 0;
        orientationData = new OrientationData();
        orientationData.register();
        //speedDisplay = (TextView) ((Activity) Constants.CURRENT_CONTEXT).findViewById(R.id.speed);
    }

    // Robot parameters getter
    public String getSpeed() {
        return this.speed;
    }

    public String getDirection() {
        return this.direction;
    }

    public String getFire() {
        return this.fire;
    }

    public int getShot() { return  this.shot;}

    public int getTurret_pos(){return  this.turret_pos;}

    public int getThrust() {
        return this.thrust;
    }

    public float getRoll() {
        float roll = orientationData.getRoll();
        return roll;
    }

    public float getYaw() {
        float yaw = orientationData.getYaw();
        return yaw;
    }

    // Robot parameters setter
    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void setShot(int shot) {this.shot = shot;}

    public void setFire(String fire) {
        this.fire = fire;
    }

    public void setSpeed(int speed) {
        this.speed = String.valueOf(speed);
    }

    public void setThrust(int thrust) {
        this.thrust = thrust;
    }

    public void setTurret_pos(int turret_pos){this.turret_pos = turret_pos;}

    public void setMove(int move){this.move = move;}

    /* This method implement the message creation. We process some robot
     * parameters and generate a string of a determined length which will be 
     * sent through TCP/IP connection
     */
    public String generateMessage() {
        int thrust = 0;
        String direction = "0";
        int yaw = 0;
        String LR = "+";

        if (Constants.GAME_MODE == 0) {
        // if in sensor mode
            if (Math.abs(this.getRoll()) < 5) { // allow small moves around initial position
                direction = "0";
                thrust = 0;
            }

            else {
                if (this.getRoll() > 0) { // robot going forward
                    direction = "1";
                    // converting thrust in % of max speed
                    thrust = (int) Math.round(2*this.getRoll());

                } else {                  // robot going backward
                    direction = "2";
                    thrust = (int) -Math.round(2 * this.getRoll());
                }
                    // make sure that the thrust is not over 99% for TCP/IP connection
                thrust = Math.min(99, thrust);
            }
        }
        else {
            //if in button mode
            thrust = this.getThrust();
            direction = this.getDirection();
        }

        if (this.getYaw() < 0) {
            LR = "";
        }

        String message;

        if (this.move == 1){
            message = String.valueOf(thrust) + "|" + direction + "|" + LR + String.valueOf((int)this.getYaw()) + "|" + this.getFire()+'|'+String.valueOf(this.getTurret_pos());
        }
        else {  // safety measure. If no one is holding the phone, the robot stops
            message = "0" + "|" + "0" + "|" + LR +  "0" + "|" + this.getFire()+'|'+String.valueOf(this.getTurret_pos());
        }
        return (message);
    }
}
