package com.lskr.mmolskrgapplication.Connection;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;
import com.lskr.mmolskrgapplication.Constants;
import com.lskr.mmolskrgapplication.R;


/**
 * Created by Julien on 28/10/2017.
 */



/**
 * This class implement an asynchronous task (a thread) which will be used to read data from the RasPi
 * avoiding blocking the UI
 */

public class ReadingTask extends AsyncTask<String,String,TcpClient> {
    public TcpClient mTcpClient;

    @Override
    protected TcpClient doInBackground(String... message) {
        mTcpClient = new TcpClient(new TcpClient.OnMessageReceived() {
            @Override
            //here the messageReceived method is implemented
            public void messageReceived(String message) {
                //this method calls the onProgressUpdate
                publishProgress(message);
            }
        });

        mTcpClient.run();

        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        if (values[0]!=""){   // if we get data from raspberry in our case being shot
            // Initialise a text object on the UI
            TextView shot = (TextView) ((Activity)Constants.CURRENT_CONTEXT).findViewById(R.id.fireMessage);
            shot.setText("SHOT");
            // make the display message on UI
            shot.setVisibility(View.VISIBLE);
        }
    }
}
