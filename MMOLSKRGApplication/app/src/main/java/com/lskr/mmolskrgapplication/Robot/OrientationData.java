package com.lskr.mmolskrgapplication.Robot;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.lskr.mmolskrgapplication.Constants;

/**
 * Created by Julien on 02/11/2017.
 *  This class implements an object used to gather the data from the phone sensors (here the
 *  accelerometer and the magnetometer)
 */

public class OrientationData implements SensorEventListener {
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private Sensor gyroscope;
    private Sensor magnometer;

    private float[] accelOutput;
    private float[] gyroOutput;
    private float[] magOutput;

    private float pitch = 0;
    public float getPitch(){return pitch*(180/(float)Math.PI);}

    private float roll = 0;
    public float getRoll(){return  roll*(180/(float)Math.PI);}

    private float yaw = 0;
    public float getYaw(){return yaw*(180/(float)Math.PI);}

    private float[] orientation = new float[3];
    public float[] getOrientation(){ return orientation;}

    private float[] startOrientation = null;
    public float[] getStartOrientation(){return startOrientation;}

    public void newGame(){ startOrientation = null;}

    // creator
    public OrientationData(){
        // Creating the sensor manager an object used to catch event from all sensors
        sensorManager = (SensorManager) Constants.CURRENT_CONTEXT.getSystemService(Context.SENSOR_SERVICE);
        // Creating the accelerometer listener
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        // Creating magnetometer listener
        magnometer     = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    public void register(){
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(this, magnometer, SensorManager.SENSOR_DELAY_GAME);
    }

    public void pause(){
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy){
    }

    // Method that will be triggered when there will be a change on sensors
    @Override
    public void onSensorChanged(SensorEvent event){
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            accelOutput = event.values;
        }
        else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD){
            magOutput = event.values;
        }
        if (accelOutput != null  &&  magOutput != null){
            // rotation matrix
            float[] R = new float[9];
            // inclination matrix
            float[] I = new float[9];
            boolean success = SensorManager.getRotationMatrix(R,I,accelOutput,magOutput);
            if (success){
                // creating the rotation matrix using the data gathered from the accelerometer
                //and the magnometer
                SensorManager.getOrientation(R,orientation);

                // if no position has been stored create our starting position
                if (startOrientation == null){
                    // copy to have two different object and not two references to the same object
                    startOrientation = new float[orientation.length];
                    System.arraycopy(orientation, 0, startOrientation, 0, orientation.length);
                }

                // calculate rotation angles by comparing the start position and the current one
                else {
                    this.pitch = orientation[1] - startOrientation[1];
                    this.roll  = orientation[2] - startOrientation[2];
                    this.yaw   = orientation[0] - startOrientation[0];
                    //System.out.println("pitch "+String.valueOf(this.pitch)+" roll "+String.valueOf(this.roll)+"yaw"+String.valueOf(this.yaw));
                }
            }
        }
    }
}
