#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Main video feed to be used with the fsm.py file.

It requires OpenCV to read the camera. However only a python2 package is 
available for the armh architecture.
Use of Python 3 for the fsm is more than recommended but to use the video 
capture, it would require to compile python-opencv for python3.

/!\ As it is, this will not work on standalone
"""

try:
    import cv2
except:
    pass
import numpy as np

def mainVideoFeed(socketVideoFeed, numCamera, stopFlag):
    """
    Main program for the video transmitter

    NB: Stop flag porbably useless if used with subprocess and not threading in
    fsm.py
    """
    print( "THREAD VIDEO OK" )

    cap = cv2.VideoCapture(numCamera)
    nocam = np.zeros([480,640,3])
    cv2.putText(nocam,"No Camera", (0,300), cv2.FONT_HERSHEY_SIMPLEX, 2, 255)

    while not stopFlag:
        # Read the camera
        ret, frame = cap.read()
        try:
            pix = frame[0,0,0]
        except:
            frame = nocam
        
        # Encode the data
        r, frame2 = cv2.imencode('.jpeg',
                                 cv2.flip(frame,1),
                                 [cv2.IMWRITE_JPEG_QUALITY, 70]
                                )
        d = frame2.flatten()
        l = d.tostring()

        # Send the data in the socket
        socketVideoFeed.send(l)
        time.sleep(0.1)

    cap.release()

