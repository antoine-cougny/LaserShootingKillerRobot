#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This class is the 'brain' of the raspberry pi

Some old statements from the switch to Python3 or how to make it work with
Python2 are still present.

It would be a good thing to try to go back to the use of treading.Thread
instead of multiprocessing.Process as it is more flexible.

Using Python3 and the video socket requires to manually compile OpenCV for 
Python3.

@date:   2017
@author: ant0ine
"""

import socket
# import threading # Not used anymore. Probably more reliable than Process
import time
import sys
import struct
from multiprocessing import Process, Queue
from getOrders import mainGetOrders
from videoFeed import mainVideoFeed
try:
    import cv2
    CV2_AVAILABLE = True
except:
    CV2_AVAILABLE = False
    print('\nOpenCV is not available!\n')

try:
    import smbus
    SMBUS_AVAILABLE = True
except:
    SMBUS_AVAILABLE = False
    print('\nNOT ON RaspberryPi!\nThe SMBUS Module is not available!\n')

class FSM(threading.Thread):
    """FSM Class for the main process"""

    ## Socket/thread codes
    VIDEO_THREAD    = 'v'
    RECEIVER_SOCKET = 'o'
    LOGS_SOCKET     = 'l'

    ## Error codes
    ERROR_CV2   = 'BAD_CV2'
    ERROR_SMBUS = 'BAD_SMBUS'
    
    ## I2C Addresses
    ADDR_MOTORCONTR = 0x03
    ADDR_GYRO       = 0x68
    ADDR_LASER      = 0x05
    ADDR_TURRET     = 0x04

    def __init__(self, numberOfSocket, 
                 group=None, target=None, name=None, 
                 args=(), kwargs=None, verbose=None
                ):
        """Initialize the object, connect to the socket, launch threads"""
        
        ## Call the init of the thrading class
        super(FSM, self).__init__(name=name, group=group, target=target, 
                                  args=args, kwargs=kwargs) 
        
        
        ## Socket variables
        self.tcpsock             = None
        self.socketSendLogs      = None
        self.socketVideoFeed     = None
        self.socketReceiveOrders = None

        self.listOfSockets  = [None]*numberOfSocket
        self.numberOfSocket = numberOfSocket
        # 0 means disconnected, 1 means connected
        self.socketStatus   = [0]*numberOfSocket
        self.numberOfSocketConnected = 0
        
        ## Thread variables
        self.threadSendLogs      = None
        self.threadVideoFeed     = None
        self.threadReceiveOrders = None

        self.timeoutQ      = Queue(5)
        self.OrderQ        = Queue(5)
        self.stop          = False
        self.stopVideo     = False
        self.stopLogs      = False
        self.stopGetOrders = False

        self.status = 'initNetwork'

    def run(self):
        t1 = time.time()
        while not self.stop: 
            if self.status == 'initNetwork':
                print("~~~~~~~~ INIT Network")
                self.initNetwork()

            if self.status == 'initSocket':
                print("~~~~~~~~ INIT Socket")
                t1 = self.initSockets()

            if self.status == 'shutdownProcedure' or self.status == 'timedOut':
                print("~~~~~~~~ SHUTDOWN")
                self.shutdownProcedure()

            if self.status == 'resetSockets':
                print("~~~~~~~~ RESET SOCKETS")
                self.resetSockets()

            if self.status == 'running':
                print("~~~~~~~~ Running")
                t1 = self.mainMonitoring(t1)
    
    def initNetwork(self):
        """
        Method used to bind the program to be able to open a socket later
        """
        ## Set up the connection listener
        self.tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        port = 1111 
        self.tcpsock.bind(("", port))

        print('Binding Port: ' + str(port))

        self.status = 'initSocket'


    def initSockets(self):
        """
        Method used to wait for the socket creation and the launching of the 
        corresponding threads.
        """
        self.tcpsock.listen(5)

        ## We got a connection
        (clientsocket, (ip, port)) = self.tcpsock.accept()
        print('New connection from: ' + str(ip) + ' on port number: ' + str(port))

        ## We read a single letter to know on which purpose the socket will be
        ## used.
        typeOfThread = clientsocket.recv(1)
        ## Read one byte to flush the \n character from the socket buffer.
        clientsocket.recv(1)
        print('Type of thread: ',typeOfThread)
        ## Conversion needed if use of Python 3
        if sys.version_info[0] >= 3:
            # typeOfThread = struct.unpack('!', typeOfThread)
            typeOfThread = chr(typeOfThread[0])

        ## Video Thread / process
        if typeOfThread == FSM.VIDEO_THREAD:
            print('  Socket to send Video feed is connected')
            if CV2_AVAILABLE:
                numCamera = 0 # Default Camera TODO: send which camera we want
                              # to use
                self.socketVideoFeed = clientsocket
                # self.threadVideoFeed = threading.Thread(name='Video_Transmitter',
                self.threadVideoFeed = Process(name='Video_Transmitter',
                                               target=mainVideoFeed,
                                               args=(self.socketVideoFeed,
                                                     numCamera,
                                                     self.stopVideo
                                                    )
                                              )
                # self.threadVideoFeed.start()
            else:
                clientsocket.sendall(FSM.ERROR_CV2)
            self.socketStatus[self.numberOfSocketConnected] = 1
            self.numberOfSocketConnected += 1
        
        ## Socket used to get orders from the phone 
        if typeOfThread == FSM.RECEIVER_SOCKET:
            print('  Socket to get orders is connected')
            self.socketReceiveOrders = clientsocket
            self.socketStatus[self.numberOfSocketConnected] = 1
            self.numberOfSocketConnected += 1

            self.threadReceiveOrders = Process(name='Rover_Get_Orders',
                                               target=mainGetOrders,
                                               args=(self.socketReceiveOrders,
                                                     self.OrderQ, 
                                                     self.timeoutQ,
                                                     self.stopGetOrders
                                                    )
                                              )
            self.threadReceiveOrders.start()
        
        ## Socket used to send status information to the phone
        if typeOfThread == FSM.LOGS_SOCKET:
            print('  Socket to send logs is connected')
            self.socketSendLogs = clientsocket
            self.socketStatus[self.numberOfSocketConnected] = 1
            self.numberOfSocketConnected +=1

            self.threadSendLogs = Process(name='Rover_Send_Logs',
                                          target=mainInteractions,
                                          args=(self.socketSendLogs,
                                                self.OrderQ,
                                                self.stopLogs
                                               )
                                         )
            self.threadSendLogs.start()

        ## Update the FSM status
        # print(self.socketStatus, all(self.socketStatus))
        if all(self.socketStatus):
            self.status = 'running'
        else:
            self.status = 'initSocket'
        time.sleep(0.1)
        return time.time()

    def resetSockets(self):
        """
        Reset all socket variables and stop flags to make the FSM available for
        a new connection.
        """
        self.numberOfSocketConnected = 0
        self.socketStatus        = [0]*self.numberOfSocket
        self.socketVideoFeed     = None
        self.socketSendLogs      = None
        self.socketReceiveOrders = None

        self.stop          = False
        self.stopVideo     = False
        self.stopLogs      = False
        self.stopGetOrders = False

        self.status ='initSocket'

    def shutdownProcedure(self):
        """
        Sockets and processes are killed one after the other to reset
        or shutdown the system.
        TODO: fix flag bahavior. maybe go back to threading.Thread object as
        we do not use python 2 anymore.
        TODO: clean dirty code related to Process, threading object & closure
        of sockets.
        """
        self.stopVideo = True
        if self.threadVideoFeed is not None and self.threadVideoFeed.is_alive():
            self.threadVideoFeed.terminate()
            time.sleep(0.05)
            self.threadVideoFeed.join()
            # self.socketVideoFeed.shutdown(socket.SHUT_RDWR)#close()
            time.sleep(0.05)
            # self.socketVideoFeed.close()
        # self.socketSendLogs.sendall('Closed')
        print('CLOSED 1')

        self.stopGetOrders = True
        if self.threadReceiveOrders is not None and self.threadReceiveOrders.is_alive():
            self.threadReceiveOrders.terminate()
            time.sleep(0.05)
            self.threadReceiveOrders.join()
        # self.socketReceiveOrders.shutdown(socket.SHUT_RDWR)#.close()
        # self.socketSendLogs.sendall('Closed')
        print('CLOSED 2')

        self.stopLogs = True
        if self.threadSendLogs is not None and self.threadReceiveOrders.is_alive():
            self.threadSendLogs.terminate()
            time.sleep(0.05)
            self.threadSendLogs.join()
        # self.socketSendLogs.sendall('Bye')
        try:
            self.socketSendLogs.shutdown(socket.SHUT_RDWR)#.close()
            print("CLOSING SOCKET")
        except:
            pass
        print('CLOSED 3')

        time.sleep(1)
        # self.socketReceiveOrders.close()
        self.socketSendLogs.close()

        if self.status == 'shutdownProcedure':
            self.status = 'closed'
            self.stop = True
        elif self.status == 'timedOut':
            self.status = 'resetSockets'
            self.stop = False

    def mainMonitoring(self, t1):
        """
        Main thread to handle reading in the socket when we get orders.
        The read function is locking the thread. Using a queue object prevents
        the mainInteractions thread to be locked.
        """
        if not self.timeoutQ.empty():
            # We empty the queue to check the state of the connection
            # This used for the time-out safety feature.
            # Every time we get an order from the socket, a number is added to 
            # this queue.
            qSize = self.timeoutQ.qsize()
            for i in range(qSize):
                self.timeoutQ.get()
            t1 = time.time()

        if (time.time() - t1) > 2:
            # We did not get any message for 2 sec
            # This is a ping timed out
            print('#########\tSHUTDOWN')
            self.status = 'timedOut'
            return t1

        elif (time.time() - t1) > 0.5:
            print('More than 1 sec without any messages, shutdoww of the motor')
            # We did not get any message for 0.5 sec, so we stop the rover.
            self.OrderQ.put('0|0|0|0|0') 
        
        self.status = 'running'
        time.sleep(0.1)
        return t1

def unpackOrder(strOrder):
    """
    Unpack an order string and return a 2D list.
    Speed[INT] | Direction[0/1/2] | AngleVariation[INT] | Fire | Up/Down [0/1/2]
        xx     |        x         |         yxx         |   x  |    x 

    """
    orderList = strOrder.split('|')

    return orderList

def mainInteractions(socketSendLogs, OrderQ, stopFlag):
    print( 'THREAD INTERACTIONS OK' )
    
    ## Check that smbus has been imported (ie we are on a RPi)
    if 'smbus' in sys.modules or SMBUS_AVAILABLE:
        ## Setup i2c
        bus = smbus.SMBus(1)
        
        # Initialization for later use
        directionRight   = '0'
        directionLeft    = '0'
        armBitLaser      = 0
        motorAndGyroData = [0, 0, 0] # Used to send to the laser canon

        while not stopFlag:
            ## Get orders
            if not OrderQ.empty():
                order = OrderQ.get()
                orderUnpacked = unpackOrder(order)

                #############################################
                #####           MOTOR CONTROLLER        #####
                #############################################
                # An offset of 50 is added not to send a too low value to the
                # motor controller. 
                speed = 2*int(orderUnpacked[0]) + 50

                # Left Motor: Make sure the value is between 50 and 250.
                speedLeft = int(speed) + int(float(orderUnpacked[2]))
                speedLeft = int(max(speedLeft, 50))
                speedLeft = min(speedLeft, 250)

                # Right Motor
                speedRight = int(speed) - int(float(orderUnpacked[2]))
                speedRight = int(max(speedRight, 50))
                speedRight = min(speedRight, 250)

                # Armbit management
                if orderUnpacked[1] == '1' or orderUnpacked[1] == '2':
                    # We move
                    armBit = '1'
                if orderUnpacked[0] == '0' or orderUnpacked[1] == '0':
                    # Speed at zero or direction 0
                    armBit = '0'
                if armBitLaser:
                    # We have been hit
                    armBit = '0'

                # Foward / backward bit handler
                if orderUnpacked[1] == '1':
                    directionLeft  = '1'
                    directionRight = '1'
                elif orderUnpacked[1] == '2':
                    directionLeft  = '0'
                    directionRight = '0'
                binarizedInfo  = int('00000' + directionLeft \
                                             + directionRight \
                                             + armBit,
                                     2
                                    )
                print(bin(binarizedInfo))
                
                ## Send Data to the motor controller through i2c
                print(binarizedInfo, speedRight, speedLeft)
                try:
                    bus.write_i2c_block_data(FSM.ADDR_MOTORCONTR, 
                                             speedRight, 
                                             [speedLeft, binarizedInfo]
                                            )
                except OSError: 
                    print('\t\tERROR IO Motor Controller')

                #############################################
                #####           GYRO CONTROLLER         #####
                #############################################
                # TODO: read values and send them to arduino Laser canon

                #############################################
                #####           LASER CONTROLLER        #####
                #############################################
                # Motor of Laser Canon Management
                # Up or down
                motorAndGyroData[0] = int(orderUnpacked[-1])
                # Fire ?
                fireLaser = int(orderUnpacked[3])

                try:
                    bus.write_i2c_block_data(FSM.ADDR_LASER, 
                                                fireLaser,
                                                motorAndGyroData
                                            )
                    if fireLaser:
                        print('\t\t\tFIRE !!!! ')
                except OSError:
                    print('\t\tNo canon !')
                    pass

                #############################################
                #####           LASER TURRET            #####
                #############################################
                try:
                    hitStatus = bus.read_byte_data(FSM.ADDR_TURRET, 0)
                    if int(hitStatus):
                        print('  The rover has been hit!')
                        armBitLaser = 1
                        # Tell the app that we have been hit
                        socketSendLogs.send(b'1\n')
                    else:
                        armBitLaser = 0
                except OSError:
                    print ('\t\tNo turret ')

            else:
                ## We wait some time to get an order message in the queue
                time.sleep(0.01)
    else:
        ## Send message if not on a Raspberry Pi
        socketSendLogs.sendall(b'BAD_SMBUS\n')
        while not stopFlag:
            # print('\t\tI')
            if not OrderQ.empty():
                order = OrderQ.get()
            time.sleep(0.01)
            # socketSendLogs.send(b'1\n')

if __name__ == '__main__':
    fsm = FSM(2)
    fsm.start()
