#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import socket
import struct
import sys

TCP_IP = ''
TCP_PORT = int(sys.argv[1])#13083#5001
filepath = sys.argv[2]

BUFFER_SIZE = 1024

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((TCP_IP, TCP_PORT))
    s.listen(1)
except:
    print("Can not bind server on port: "+ str(TCP_PORT) +"\n")

print("Wait for connections!!!\n")
conn, addr = s.accept()
print("Receive a new connection!!!\n")

# Read the file and store the binary data
f = open(filepath, 'rb')
dataRaw = f.read()
f.close()

# Send total size of file
fileSize = len(dataRaw)
sizeToSend = struct.pack('!i', fileSize)
print('size:', fileSize)
conn.sendall(sizeToSend)

# Send file by chunks of length BUFFER_SIZE 
print('Starting sending the file')
size = fileSize

while size > BUFFER_SIZE:
    conn.sendall(dataRaw[fileSize - size: fileSize - size + BUFFER_SIZE])
    size -= BUFFER_SIZE

# Send the last part of dataRaw which will be inferior than BUFFER_SIZE
conn.sendall(dataRaw[fileSize - size: -1])
print('End of transmission')

# Close the socket
conn.close()
