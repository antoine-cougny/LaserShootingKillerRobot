#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import socket           # For the connection
import threading        # Multi threading
import cv2              # Image & Camera processing
import time         
import numpy as np      # Array management
import struct           # Pack data in bytes
# import maestro
from serial import *    # Connection to the board. Maybe I2C ?


MOTORCONTR = '!bbb'

class ServerRPi(threading.Thread):
    def __init__(self, ip, port, clientsocket):
        threading.Thread.__init__(self)
        self.ip           = ip
        self.port         = port
        self.clientsocket = clientsocket
        self.blackframe   = np.zeros([480,640,3])

    def run(self):
        print("Connection from %s:%s" %(self.ip, self.port,))
        # Read what you need to send
        r = self.clientsocket.recv(2048)

        # If we are asked to send the camera feed
        if r=="image":
            cap = cv2.VideoCapture(0)
            nocam = np.zeros([480,640,3])
            cv2.putText(nocam,"No Camera", (0,300), cv2.FONT_HERSHEY_SIMPLEX, 2, 255)

            while True:
                ret, frame = cap.read()
                try:
                    pix = frame[0,0,0]
                except:
                    frame = nocam
                
                r, frame2 = cv2.imencode('.jpeg',
                                         cv2.flip(frame,1),
                                         [cv2.IMWRITE_JPEG_QUALITY, 70]
                                        )
                d = frame2.flatten()
                l = d.tostring()
                self.clientsocket.send(l)
                time.sleep(0.1)
            cap.release()

        elif r=="control":
            maescon=True
            print('control')
            # try:
            #     self.servo = maestro.Controller()
            # except:
            #     maescon=False
            if maescon:
                # self.servo.setAccel(0,10)
                # self.servo.setAccel(1,10)
                # d=2000
                # g=2000
                while True:
                    r = self.clientsocket.recv(2048)
                    # self.servo.setTarget(0,5600)
                    # self.servo.setTarget(1,5600)
                    time.sleep(0.1)
                    if r=="z" or r=='w':
                        d,g=4000,4000
                        print('forward')
                    if r=="q":
                        d,g=4000,0
                        print('left')
                    if r=="d":
                        d,g=0,4000
                        print('right')
                    if r=="s":
                        d,g=0,0
                        print('backward')
                    if r=="a":
                        d,g=1600,1600 
                        print('stop')
                    # if r=="y":
                    #     self.stop()
                    # self.servo.setTarget(0,d+4000)
                    # self.servo.setTarget(1,g+4000)
                    time.sleep(0.02)


if __name__ == '__main__':
    # Set up the connection listener
    tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    tcpsock.bind(("",1111))

    while True:
        # Listen if a client tries to connect
        tcpsock.listen(10)
        print("Listening...")
        (clientsocket, (ip, port)) = tcpsock.accept()
        # A client is now connected
        newthread = ServerRPi(ip, port, clientsocket)
        newthread.start() 
