#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
To be used on a Raspberry Pi 3
Test of the motor controller using Python.
The motors should turn randomly as the speed are randomly generated.
"""
import smbus
import struct
import time
import random

bus = smbus.SMBus(1)
addr = 0x03


for k in range(80):
    a = int('00101100'+'00101001'+'00001111', 2)
    #bus.write_byte(addr, a)
    a = random.randint(50, 240)
    b = random.randint(50, 240)
    bus.write_i2c_block_data(addr, a, [b, 1])
    print(k, a, b)
    time.sleep(0.2)
