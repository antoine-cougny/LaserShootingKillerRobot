#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('./src/')

from fsm import FSM

if __name__ == '__main__':
    fsm = FSM(2)
    fsm.start()
